'use strict';
import React from 'react';
import AddSingle from './add-single/AddSingle';

export default function () {
    return AllowedContactsRender.call(this);
}

var AllowedContactsRender = function () {
    return (
        <div
            className="main-content email-accounts col-xs-12 col-sm-offset-4 col-sm-7 col-md-offset-3 col-md-8 whiteBg">
            <h2>Allowed Contacts</h2>

            <h3>- Add contacts to this list to receive notifications from them.</h3>

            <button className="blueBg btn btn-info col-xs-12 col-md-3 weight300" onClick={this.addContact}>
                AUTO DETECT<i className="fa fa-user-plus" aria-hidden="true"></i>
            </button>
            <button className="blueBg btn btn-info col-xs-12 col-md-offset-1 col-md-3 weight300 border-less"
                    onClick={()=>{this.setState({showAddSingle:true}) } }>
                ADD SINGLE<i className="fa fa-user-plus" aria-hidden="true"></i>
            </button>
            <button className="blueBg btn btn-info col-xs-12 col-md-offset-1 col-md-3 weight300 border-less"
                    onClick={this.toggleSearchBox}>SEARCH <i className="fa fa-search" aria-hidden="true"></i></button>

            <input autoFocus={true} onChange={(e)=>{this.filerAllowedContacts(e.target.value)}}
                   value={this.state.searchString} type="text" ref="searchText" placeholder="Search..."
                   className={this.state.showSearchBox ? 'show col-xs-12 p-l-5 font-size22 m-t-20' : 'hide'  }/>

            <div className="col-xs-12 p-a-0 m-t-20">
                {/*<div className="col-xs-4 p-a-10  weight400 emaillists">Name</div>*/}
                <div className="col-xs-12 p-a-10 p-l-0  weight400 emaillists">EMAILS</div>
                {this.state.allowedContacts.map((contact, index) => (
                    <div key={contact.email} className='col-xs-12 p-a-10  weight400 emaillists'>
                        {
                            //<div className="col-xs-4 weight400 p-t-10  p-b-10 p-l-0 word-break">{contact.name}</div>
                        }
                        <div className="col-xs-10 weight400 p-t-10  p-b-10 p-l-0 word-break">{contact.email}</div>
                        <div className="col-xs-2 weight400 p-t-10  p-b-10 p-l-20">
                            <i onClick={this.removeContact.bind(this, contact)}
                               className="fa fa-times fa-2x pointer pull-right" aria-hidden="true" title="Delete"></i>
                        </div>
                    </div>
                ))}
            </div>

            <AddSingle showAddSingle={this.state.showAddSingle} addSingle={this.addSingle}
                       closeModal={this.closeModal}/>
        </div>
    )
};
