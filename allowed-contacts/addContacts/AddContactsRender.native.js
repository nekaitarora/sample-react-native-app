'use strict';
import React from 'react';
import { View, ScrollView, Modal , TouchableHighlight} from 'react-native';
import { Button, Text, Icon, List, ListItem, CheckBox, Spinner ,InputGroup, Input, Picker} from 'native-base';
import Menu, { MenuContext, MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';
import AddSingle from '../add-single/AddSingle';
import styles from '../../../../www/css/style';
import { detect } from '../../../common/react-cross-platform';

const Item = Picker.Item;

export default function () {
    return AddContacts.call(this);
}

var AddContacts = function () {

    let picker = () => {
        let ios = detect.ios;

        return (
            <View style={[ios ? styles.padding15 :{}]}>
                {
                    ios ? (
                        <Menu onSelect={(value) => {this.getContacts(value)} }>
                            <MenuTrigger style={[styles.flexRow, styles.justifySpace, styles.alignCenter]}>
                                <Text style={[styles.font16]}>
                                    {this.state.currentEmail}
                                </Text>
                                <Icon name="md-arrow-dropdown" color="black" style={styles.font18}/>
                            </MenuTrigger>

                            <MenuOptions optionsContainerStyle={[ styles.widthMax, styles.padding10 ]}>
                                {
                                    this.state.emailAccounts.map((account, index) => (
                                        <MenuOption key={index} value={index}>
                                            <Text style={[styles.font16]}>{account.email}</Text>
                                        </MenuOption>
                                    ))
                                }
                            </MenuOptions>
                        </Menu>
                    ) : (
                        <Picker
                            mode="dropdown"
                            iosHeader={"Select email account"}
                            selectedValue={this.state.currentEmailIndex}
                            onValueChange={ value => { this.getContacts(value); } }>
                            {this.state.emailAccounts.map((account, index) => (
                                <Item key={index} value={index} label={account.email}/>
                            ))}
                        </Picker>
                    )
                }
            </View>
        )
    }

    return (
        <MenuContext style={[styles.flex1,styles.margin2]}>
            <Text style={[styles.heading, styles.m_t_5]}>Add Contacts</Text>
            <Text style={[styles.subheading]}>
                - Select your email account for contacts
            </Text>
            {
                detect.android ? picker()
                    : this.state.emailAccounts && this.state.emailAccounts.length > 0 && picker()
            }

            <View style={[styles.btns,styles.m_b_5]}>
                <Button small iconRight style={styles.btn} onPress={this.saveContacts}><Icon
                    name="md-checkmark"/>Save</Button>
                <Button small iconRight style={styles.btn}
                        onPress={()=>{this.setState({showAddSingle:true})}}><Icon
                    name="md-person-add"/>Add Single</Button>
                <Button small iconRight style={styles.btn}
                        onPress={this.toggleSearch}><Icon
                    name="ios-search"/>Search</Button>
            </View>

            {/*showing serach box here via toggle ....*/}
            {this.state.showSearchBox ?
                <InputGroup borderType='underline' style>
                    <Icon name="ios-search" style={styles.icon}/>
                    <Input autoFocus={true} onChangeText={(e)=>{ this.filterContacts(e); } }
                           autoCapitalize={'none'} value={this.state.searchText} placeholder='Search...'/>
                </InputGroup>
                : null}
            {/*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/}

            <List style={styles.m_t_2}>
                <ListItem iconLeft iconRight style={styles.listHeader}>
                    <Text> Contacts </Text>
                </ListItem>
            </List>
            {/* showing infinte scroll list of contacts*/}
            {
                this.state.contacts && this.state.contacts.length > 0 ?
                    <ScrollView
                        onScroll={this.appendContacts}
                        scrollEventThrottle={20}
                        ref={ ref => this.listOfContacts = ref}>
                        <List style={[styles.m_t_2]}
                              dataArray={this.state.contacts}
                              renderRow={(contact, index) =>
                                    <ListItem onPress={this.selected.bind(this,contact)}>
                                        <View style={styles.flexRow}>
                                            <CheckBox
                                                checked={ _.find(this.selectedList, {email : contact.email}) ? true : false }
                                                onPress={this.selected.bind(this,contact)}/>
                                            <View style={[styles.justifyCenter, styles.m_l_20]}>
                                                <Text style={styles.font14}>{contact.email}</Text>
                                                <Text style={styles.l20}>{contact.name}</Text>
                                            </View>
                                        </View>
                                    </ListItem>
                                    }>
                        </List>
                    </ScrollView>
                    : <View style={[styles.alignCenter, styles.m_t_20]}>
                        <Icon name="md-warning" style={styles.icon}/>
                        <Text style={[styles.textCenter, styles.font16,styles.m_t_10]}>
                            {`There is no contacts for "${this.state.currentEmail}".`}
                        </Text>
                    </View>

            }

            <AddSingle showAddSingle={this.state.showAddSingle} addSingle={this.addSingle}
                       closeModal={this.closeModal}/>

            {
                //<Modal visible={this.state.showFirstVisit}
                //       onRequestClose={this.closeModal.bind(this,'firstVisit')}
                //       animationType={'fade'} transparent={true}>
                //    <View style={[styles.container, styles.opacity100]}>
                //        <Text style={styles.heading}>Congratulations for successful setup.</Text>
                //        <Button block style={styles.btn} onPress={this.closeModal.bind(this,'firstVisit')}
                //                iconRight><Icon
                //            name='md-close'/> Close</Button>
                //    </View>
                //</Modal>
            }
        </MenuContext>
    )
}
