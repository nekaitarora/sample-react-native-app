'use strict';
import React from 'react';
import Modal from 'react-modal';
import AddSingle from '../add-single/AddSingle';
import Loading from "../../../subcomponents/loading/Loading";

export default function () {
    return AddContacts.call(this);
}

var AddContacts = function () {
    return (
        <Modal className="react-modal modal-contacts m-t-0 m-b-20" overlayClassName="react-overlay"
               isOpen={this.state.open}
               onRequestClose={this.closeModal} shouldCloseOnOverlayClick={false}>

            <div className={ this.state.shouldCloseModal ? 'close' : 'hide'}>
                <i className="fa fa-times fa-3x blueColor" onClick={this.closeModal}></i>
            </div>

            <h1>Add Contacts</h1>

            <div className="col-xs-12 p-a-0">
                <div className="col-md-5 col-sm-5 p-a-0 m-t-5">
                    <h4 className="m-a-0">- Select your email account for contacts</h4>
                </div>
                <div className="col-md-7 col-sm-7 p-a-0">
                    <select className="form-control" onChange={e => {this.getContacts(e.target.value)}}
                            defaultValue={this.state.currentEmailIndex}>
                        {this.state.emailAccounts.map((account, index) => (
                            <option key={index} value={index}>{account.email}</option>
                        ))}
                    </select>
                </div>
            </div>

            <h3>- Click to select the contacts you would like to add.</h3>

            <div className="row">
                <div className="col-xs-12 col-sm-3 col-md-3">
                    <button className="blueBg btn btn-info col-xs-12" onClick={this.saveContacts}>
                        SAVE CHANGES <i className="fa fa-check m-r-5"></i>
                    </button>
                </div>
                <div className="col-xs-12 col-sm-3 col-md-3">
                    <button className="blueBg btn btn-info col-xs-12 border-less"
                            onClick={()=>{this.setState({showAddSingle:true}) } }>
                        ADD SINGLE <i className="fa fa-user-plus" aria-hidden="true"></i>
                    </button>
                </div>
                <div className="col-xs-12 col-sm-3 col-md-3">
                    <button className="blueBg btn btn-info col-xs-12 border-less" onClick={this.toggleSearch}> SEARCH <i
                        className="fa fa-search" aria-hidden="true"></i>
                    </button>
                </div>
            </div>

            <div className="row">
                <input type="text" onChange={(e)=>{this.filterContacts(e.target.value)}} value={this.state.searchText}
                       className={ this.state.showSearchBox ? "form-control" : 'hide' }
                       placeholder="Enter Text To Search" ref="searchText"/>
            </div>

            {/* list of contacts... */}
            <div className="row add-contacts email-accounts">
                <div className="col-xs-12 m-t-15 m-b-20" id="listOfContacts" style={{overflow:'auto',height:300 }}
                     onScroll={this.appendContacts}>
                    <div className="col-xs-12 p-a-0 list-header emaillists">
                        <div className="col-xs-4 weight400 p-a-10">NAME</div>
                        <div className="col-xs-7 col-xs-offset-1 weight400 p-a-10">EMAIL</div>
                    </div>

                    {this.state.contacts.map((list, index) => (
                        <div
                            className={ _.find(this.selectedList,{email : list.email}) ? 'selected col-xs-12 p-a-0 list-body pointer emaillists' : 'col-xs-12 p-a-0 list-body pointer emaillists'}
                            key={index} onClick={this.selected.bind(this, list) }>
                            <div className="col-xs-4 weight400 p-a-10 word-break">{list.name}</div>
                            <div className="col-xs-7 col-xs-offset-1 weight400 p-a-10 word-break">{list.email}</div>
                        </div>
                    ))}
                </div>
            </div>

            <AddSingle showAddSingle={this.state.showAddSingle} addSingle={this.addSingle}
                       closeModal={this.closeModal}/>
            {
                //<Modal className="react-modal modal-contacts modal-subject" overlayClassName="react-overlay"
                //       isOpen={this.state.showFirstVisit} onRequestClose={this.closeModal.bind(this,'firstVisit')}>
                //    <div className="text-center p-a-25">
                //        <h3 className="blueColor">Congratulations for successful setup.</h3>
                //        <h4>An email has been sent to your box with the application link. "Check your mobile device
                //            email
                //            for the app link".</h4>
                //        <button className="btn btn-info blueBg" onClick={this.closeModal}>Close<i
                //            className="fa fa-close"
                //            aria-hidden="true"></i>
                //        </button>
                //    </div>
                //</Modal>
            }
        </Modal>
    )
};
