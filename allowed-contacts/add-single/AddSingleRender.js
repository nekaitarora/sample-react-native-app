'use strict';
import React from 'react';
import Modal from 'react-modal';

export default function () {
    return AddSingle.call(this);
}

var AddSingle = function () {
    return (
        <Modal className="react-modal modal-contacts modal-subject" overlayClassName="react-overlay"
               isOpen={this.props.showAddSingle} onRequestClose={this.props.closeModal.bind(this,'inner')}>
            <h3>Enter the Email you would like to receive notifications for:</h3>

            <div className="row">
                {
                    //<input type="text" ref="name" onChange={(e) => {this.setState({name: e.target.value})}}
                    //       placeholder="Enter Name" className="col-xs-12"
                    //       onKeyPress={this.handleKeyPress}/>
                }
                <input type="email" onChange={(e) => {this.setState({email: e.target.value})}}
                       placeholder="Enter Email..." className="col-xs-12" ref="email"
                       onKeyPress={this.handleKeyPress}/>
                <button onClick={this.addSingle} className="col-xs-11 blueBg btn btn-info m-a-25">Submit<i
                    className="fa fa-check"></i></button>
            </div>
        </Modal>
    )
};
