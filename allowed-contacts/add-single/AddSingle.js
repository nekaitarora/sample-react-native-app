import AddSingleBase  from './AddSingleBase';
import AddSingleRender  from './AddSingleRender';

export default class AddSingle extends AddSingleBase {
    render() {
        return AddSingleRender.call(this, this.props, this.state);
    }
}
