'use strict';
import React,{Component} from 'react';
import lodash from 'lodash';
import validator from 'validator';
import api from '../../../common/api';
import {detect, notify, storage, navigate} from '../../../common/react-cross-platform';
import * as redux from '../../../redux/redux';

class AddSingle extends Component {
    constructor(props) {
        super(props);

        this.state = {email: ''};

        this.addSingle = this.addSingle.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleKeyPress(e) {
        if (e.key == 'Enter') {
            this.addSingle(e);
        }
    }

    addSingle() {
        this.props.addSingle(this.state);
    }
}

module.exports = AddSingle;
