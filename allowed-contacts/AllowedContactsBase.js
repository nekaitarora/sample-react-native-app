'use strict';
import React,{Component} from 'react';
import lodash from 'lodash';
import validator from 'validator';
import api from '../../common/api';
import MailStore from '../../redux/mailStore';
import { addUser } from '../../redux/mailActions';
import { detect, storage, notify, navigate, confirm } from '../../common/react-cross-platform';
import * as redux from '../../redux/redux';

class AllowedContacts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allowedContacts: [],
            showAddSingle: false,
            showSearchBox: false,
            searchString: "",
            loading: false
        };
        this.addContact = this.addContact.bind(this);
        this.toggleSearchBox = this.toggleSearchBox.bind(this);
        this.filerAllowedContacts = this.filerAllowedContacts.bind(this);
        this.addSingle = this.addSingle.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal(state) {
        switch (state) {
            case 'inner' :
                this.setState({showAddSingle: false});
                break;
            default :
                navigate('allowed-contacts', this.props);
                break;
        }
    }

    componentWillMount() {
        redux.getUserAsync().then(user=> {
            if (user) {
                if (!user.emailAccounts || user.emailAccounts.length == 0) {
                    navigate('welcome-wizard', this.props);
                }
                //else if (user.emailAccounts.length > 0 && user.allowedContacts.length == 0) {
                //    navigate('detect-contacts', this.props, {query: {email: user.emailAccounts[0].email}});
                //}
                else {
                    this.setState({'allowedContacts': user.allowedContacts});
                }
            } else {
                notify('User is not logged in.');
            }
            return null;
        }).catch(err => {
            console.log("err in allowed-contacts is :", err);
            navigate('login', this.props);
        });
    }

    toggleSearchBox() {
        redux.getUserAsync().then(user=> {
            if (user) {
                this.setState({
                    allowedContacts: user.allowedContacts,
                    searchString: '',
                    showSearchBox: !this.state.showSearchBox
                }, ()=> {
                    detect.web ? this.refs.searchText.focus() : "";
                });
            }
        });
    }

    filerAllowedContacts(text) {
        this.setState({searchString: text}, ()=> {
            let regex = new RegExp(text.trim(), 'i');

            redux.getUserAsync().then(user=> {
                if (user) {
                    let list = user.allowedContacts.filter((item) => {
                        return (item.name ? item.email.match(regex) || item.name.match(regex) : '' );
                    });
                    this.setState({allowedContacts: list});
                }
            });
        });
    }

    addContact() {
        redux.getUserAsync().then(user=> {
            navigate('detect-contacts', this.props, {query: {email: user.emailAccounts[0].email}});
            return null;
        }).catch(err=> {
            console.log("error is ", err)
        });
    }

    addSingle(data) {
        let { email} = data;
        //name = name.trim();
        email = email.trim().toLowerCase();

        if (!email) {
            notify('Please fill all fields.');
        } else if (!validator.isEmail(email)) {
            notify('Please provide valid email-account.');
        } else if (lodash.find(this.state.allowedContacts, {email})) {
            notify('Account has already been added.');
        } else {
            let {allowedContacts} = this.state;
            allowedContacts.push({email});
            api.post('/data/users/addContacts', allowedContacts).then(result => {
                if (result) {
                    redux.setUser(result.user);
                    this.setState({showAddSingle: false});
                } else {
                    notify('Nothing saved.');
                }
            }).catch(err => {
                console.log('error in addSingle', err);
            });
        }
    }

    removeContact(contact) {
        confirm('Are you sure you want to delete this Allowed Contact?', ()=> {
            api.post("/data/users/removeContact", {contact}).then(result => {
                redux.setUser(result.user);
                this.setState({allowedContacts: result.user.allowedContacts});
            }).catch(er => {
                console.log('err in deleting contact is ', er);
            });
        }, null);

    }

}
module.exports = AllowedContacts;
