import Base  from './DetectContactsBase';
import Render  from './DetectContactsRender';

class DetectContacts extends Base {
    render() {
        return Render.call(this, this.props, this.state);
    }
}

export default DetectContacts;
