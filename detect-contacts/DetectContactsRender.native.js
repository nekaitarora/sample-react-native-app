import React from 'react';
import {Modal, View, ScrollView, Image, TextInput} from 'react-native';
import {Button, Icon, List, ListItem, Text} from 'native-base';
import {detect} from '../../common/react-cross-platform';
import styles from '../../../www/css/style';

export default function () {
    return DetectContacts.call(this);
}

var DetectContacts = function () {
    return (
        <Modal visible={this.state.showDetectContact || detect.ios} animationType={'fade'}
               transparent={true} onRequestClose={this.closeModal}>
            <View style={[styles.container, styles.opacity95]}>
                <View style={{alignItems:'center'}}>
                    <Image style={styles.detectLogo} source={require('../../../img/man-flashlight.png')}/>
                    <Text style={[styles.heading, styles.textCenter]}>You have been successfully authenticated!</Text>
                    <Text style={[styles.subheading,styles.textCenter]}>
                        We are now detecting your contacts, please wait...
                    </Text>
                    {
                        // detect.ios &&
                        <Button block style={[styles.bgColorBlue, styles.m_t_20]} onPress={this.closeModal}
                                iconRight>
                            back <Icon name='md-arrow-round-back'/>
                        </Button>
                    }
                </View>
            </View>
        </Modal>
    );
};
