import React, {Component} from 'react';
import lodash from 'lodash';
import api from '../../common/api';
import GmailApi from '../../common/GmailApi';
import {detect, navigate, storage, notify, confirm, confirmYesNo} from '../../common/react-cross-platform';
import * as redux from '../../redux/redux';
import validator from 'validator';
import config from '../../common/config';


class DetectContactsBase extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDetectContact: false,
            shouldCloseModal: true,
        };
        this.isTimeout = true;
        this.isBackground = false;
        this.isForeground = true;

        this.closeModal = this.closeModal.bind(this);
        this.getImapContacts = this.getImapContacts.bind(this);
        this.getGmailContacts = this.getGmailContacts.bind(this);
        this.waitAndNotify = this.waitAndNotify.bind(this);
        this.checkBackgroundAndNavigate = this.checkBackgroundAndNavigate.bind(this);
    }

    componentWillMount() {
        redux.getUserAsync().then(user => {
            if (user && user.emailAccounts && user.emailAccounts.length > 0) {

                let {emailAccounts} = user;
                let {email} = this.props.location.query;
                let account = lodash.find(emailAccounts, {email: email});

                if (account && account.imapState) {
                    this.setState({showDetectContact: true}, () => {
                        if (account.type == 'gmail') {
                            this.getGmailContacts(account);
                        } else {
                            this.getImapContacts(account);
                        }
                    });
                } else if (!account.imapState) {
                    notify('Email-setings for this email-account is iincorrect, Please provide a valid email-setings.', () => {
                        navigate('email-setting', this.props, {query: {email: account.email}});
                    });
                } else {
                    notify('Please provide a valid email to get contacts.');
                    navigate('allowed-contacts', this.props);
                }
            } else {
                navigate('welcome-wizard', this.props);
            }
            return null;
        }).catch(err => {
            console.log('error in detect contacts componentWillMount', err);
            navigate('login', this.props);
        });
    }

    waitAndNotify() {
        setTimeout(() => {
            if (this.isTimeout) {
                let message = `It is taking longer than expected and we will continue in the background, and let you know as we finish.`;
                confirm(message, () => {
                    this.isBackground = true;
                    navigate('allowed-contacts', this.props);
                }, null);
            }
        }, 30 * 1000);
    }

    checkBackgroundAndNavigate(account) {
        if (this.isBackground) {
            let message = `We successfully detected contacts for "${account.email}", would you like to see them?`;

            notify(message, () => {
                navigate('add-contacts', this.props, {query: {email: account.email}});
            });
        } else if (this.isForeground) {
            navigate('add-contacts', this.props, {query: {email: account.email}});
        }
    }

    getImapContacts(account = {}) {

        if ((account.type == 'imap' && account.imapHost && account.imapPort) || account.type == 'gmail') {

            this.waitAndNotify();
            api.post('/data/users/detectImapContacts', account).then(result => {

                this.isTimeout = false;
                if (result && result.notify) {
                    notify(result.notify);
                    navigate('message-list', this.props);
                } else if (result && result.err) {
                    console.log(result.err);
                    let message = this.getErrorMessage(account.email, result.err);

                    notify(`${message}\nPress ok to go back and edit your email settings.`, () => {
                        navigate('email-setting', this.props, {query: {email: account.email}});
                    });
                } else {
                    redux.setContacts(result);
                    this.checkBackgroundAndNavigate(account);
                }
            });
        } else {
            notify('You need to provide imap setting.');
            navigate('email-setting', this.props, {query: {email: account.email}});
        }
    }



    /**
     * to get contacts of gmail account via gmail api.....
     */
    getGmailContacts(account = {}) {
        let gmailApiUrl = config.GMAIL_CONTACTS_API_URL;

        this.waitAndNotify();

        GmailApi.get(gmailApiUrl, {}, account).then((result) => {

            this.isTimeout = false;

            if (result && Array.isArray(result.feed.entry)) {
                let contacts = [];

                result.feed.entry.forEach((record) => {
                    if (record.gd$email && Array.isArray(record.gd$email)) {
                        let emailObj = record.gd$email[0];
                        let nameObj = record.gd$name;

                        let emailAddress = (emailObj && emailObj.address) ? emailObj.address : null;
                        let fullName = (nameObj && nameObj.gd$fullName && nameObj.gd$fullName.$t) ? nameObj.gd$fullName.$t : null;

                        if (validator.isEmail(emailAddress)) {
                            let newRecord = {
                                email: emailAddress.toLowerCase(),
                                name: fullName || 'Unknown'
                            };
                            contacts.push(newRecord);
                        }
                    }
                });
                redux.setContacts(contacts);

                this.checkBackgroundAndNavigate(account);

            } else {
                //notify(`No contacts have been found for "${account.email}".`);
                //navigate('allowed-contacts', this.props);
                this.checkBackgroundAndNavigate(account);
            }
            return null;
        }).catch(err => {
            console.log('error in getGmailContacts', err);
            navigate('email-accounts', this.props);
        });
    }

    closeModal() {
        this.isTimeout = false;
        this.isForeground = false;
        navigate('allowed-contacts', this.props);
    }

    getErrorMessage(email, err) {
        let message = `We were unable to connect to your email server with the settings you entered.`;

        if (err.source == 'authentication') {
            message = `Authentication failed for ${email}, please provide correct email and/or  password.`;
        } else if (err.source == 'protocol') {
            message = err && err.msg ? err.msg : `Full IMAP support is NOT enabled for ${email}.`;
        } else if (err.source == 'socket' || err.source == 'timeout' || err.source == 'timeout-auth') {
            message = `Authentication failed for ${email}, Please provide the correct IMAP Host and/or IMAP Port.`;
        }
        return message;
    }
}

module.exports = DetectContactsBase;
