import React from 'react';
import Modal from 'react-modal';

export default function () {
    return DetectContacts.call(this);
}

var DetectContacts = function () {
    return (
        <div
            className="main-content email-accounts col-xs-12 col-sm-offset-4 col-sm-7 col-md-offset-3 col-md-8  whiteBg">
            <h2>Detecting Contacts</h2>
            <Modal className="react-modal modal-contacts modal-subject" overlayClassName="react-overlay"
                   isOpen={this.state.showDetectContact} >
                <div className={ this.state.shouldCloseModal ? 'close' : 'hide'}>
                    <i className="fa fa-times fa-3x blueColor" onClick={this.closeModal}></i>
                </div>
                <div className="text-center p-a-25">
                    <img src="img/man-flashlight.png" width="400"/>

                    <h3 className="blueColor">You have been successfully authenticated!</h3>

                    <h3>We are now detecting your contacts, please wait... </h3>
                </div>
            </Modal>
        </div>
    )
};
